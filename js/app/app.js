var App = Ember.Application.create({
    LOG_TRANSITIONS: true
});

App.Router.map(function() {
    this.route('about');
});

App.IndexController = Ember.Controller.extend({
    numProductos: 3,
    logo: 'images/logo.png',
    horaActual: function(){
        return new Date().toDateString();
    }.property()
});